# Setup host

```bash
wget -O - https://gitlab.com/sebtic31/autoconfig/-/raw/main/setup-host.sh | bash
```

# Setup docker proxy for host

```bash
wget -O - https://gitlab.com/sebtic31/autoconfig/-/raw/main/setup-proxyied-docker.sh | bash
```

