#!/bin/bash

apt install mc liquidprompt sudo curl wget -y

if [ -z "$(grep liquidprompt /root/.bashrc)" ]; then
    liquidprompt_activate
fi

MAIN_USER=$(id 1000 | cut -d ' ' -f 1 | sed 's/.*(\(.*\))/\1/')
echo "$MAIN_USER ALL=NOPASSWD: ALL" > /etc/sudoers.d/$MAIN_USER

# Install SSH public key
SSH_AUTHORIZED_KEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCxXwW7Lv5HL+2rwLO7jA/7xtnJu+noDhuApx9Hgf6RUNKLkQz2jhYC/ZWdZce/RbQYYIcTxHP/Zvjgbj4btcf6l3dK4KtrL2XIr2485UsqWjHCz0hMn7m1uZzBeg76OoiUMtU6K8MbihIk8UepbKwJ9VvjHwgEkQcWm1BZ1tzalswo4Fw9jRCzgqLmCQYGlTfRerTwpxuaY7K7+abRzZEMooI50Lo8oPALvGQB9uLRtwg/A/KZgsAml3mPjaI/uUxi4HBOlCQIOodGbIdpEXnS2fVaHjPNr4ILg6PW6FIaZSz57vo4rvSMYGyEP806KfBqY5x8jcAl4T5Dzu+1SLMt proxnet"

for u in root $MAIN_USER; do
    HOMEDIR=$( getent passwd $u | cut -d: -f6 )
    
    mkdir -p $HOMEDIR/.ssh    
    touch $HOMEDIR/.ssh/authorized_keys
    chmod g-rwx $HOMEDIR/.ssh
    chmod o-rwx $HOMEDIR/.ssh

    grep -v "$SSH_AUTHORIZED_KEY" $HOMEDIR/.ssh/authorized_keys > /tmp/authorized_keys
    echo "$SSH_AUTHORIZED_KEY" >> /tmp/authorized_keys

    mv /tmp/authorized_keys $HOMEDIR/.ssh/authorized_keys
    chmod g-rwx $HOMEDIR/.ssh/authorized_keys
    chmod o-rwx $HOMEDIR/.ssh/authorized_keys

    chown $u:$u -R $HOMEDIR/.ssh
done

# enable global bash-completion
if [ -z "$(grep bash_completion /etc/bash.bashrc | grep -v '#')" ]; then
cat <<EOF >> /etc/bash.bashrc
# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
EOF
fi

if which docker; then
    echo "Docker is already installed. Skipping..."
else
    wget -O - get.docker.com | bash
fi
