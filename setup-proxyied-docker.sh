#!/bin/bash

mkdir -p /etc/systemd/system/docker.service.d
cat <<EOF > /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://docker-registry-proxy:3128/"
Environment="HTTPS_PROXY=http://docker-registry-proxy:3128/"
EOF

# Get the CA certificate from the proxy and make it a trusted root.
curl http://docker-registry-proxy:3128/ca.crt > /usr/share/ca-certificates/docker_registry_proxy.crt
echo "docker_registry_proxy.crt" >> /etc/ca-certificates.conf
update-ca-certificates --fresh

# Reload systemd
systemctl daemon-reload

# Restart dockerd
systemctl restart docker.service

# Install cron tab to refresh proxy cert
(
crontab -l | grep -v docker_registry_proxy.crt
cat << EOF
0 0 * * * wget -q -t1 -O /usr/share/ca-certificates/docker_registry_proxy.crt http://docker-registry-proxy:3128/ca.crt && (update-ca-certificates --fresh > /dev/null 2>/dev/null) && systemctl restart docker.service
EOF
) | crontab -
